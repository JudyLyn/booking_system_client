import { Button } from 'react-bootstrap'


export default function EnrollmentButton({courseId}) {
    
    const enroll = (courseId) => {
        fetch(`http://localhost:4000/api/courses/${courseId}/enrollments`, {
            method: 'PUT',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            if(data === true){
                alert('Congratulations! Your enrollment has been processed successfully.')
            }else{
                alert('Oops...', 'Something went wrong!', 'error')
            }
        })
    }
    
    return (
        <Button variant="success" onClick={() => enroll(courseId)}>Enroll</Button>    
    )
}

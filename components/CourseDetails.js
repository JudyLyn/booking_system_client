import { Jumbotron, ListGroup } from 'react-bootstrap'
import Link from 'next/link'

export default function CourseDetails({courseData}) {
    return (
        <>
            <Jumbotron>
                <h1>{courseData.name}</h1>
                <h2>{courseData.description}</h2>
                <p>Price: PhP{courseData.price}</p>
            </Jumbotron>

            {courseData.enrollees.length > 0 
            ? <>
                <h3>Enrollees</h3>
                <ListGroup>
                    {courseData.enrollees.map(enrollee => {
                        return (
                            <ListGroup.Item key={enrollee._id}>
                                <Link href="/users/[id]" as={`/users/${enrollee.userId}`}>
                                        <a>{enrollee.userId}</a>
                                </Link>
                            </ListGroup.Item>
                        )
                    })}
                </ListGroup>
            </>
            : null}
        </>
    )
}

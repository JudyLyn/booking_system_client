import { Button } from 'react-bootstrap'
import Router from 'next/router'

export default function UpdateButton({courseId}) {
    const dirToEditForm = (courseId) => {
        Router.push({
        	pathname: '/courses/edit',
        	query: {courseId: courseId}
        })
    }
    return (
        <Button variant="warning" onClick={() => dirToEditForm(courseId)}>Update</Button>
    )
}

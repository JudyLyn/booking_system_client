import { Table } from 'react-bootstrap'
import UpdateButton from './UpdateButton'
import ArchiveButton from './ArchiveButton'
import Link from 'next/link'

export default function AdminCourseTable({courses}) {
    return (
        <Table>
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Status</th>
                    <th>Description</th>
                    <th>Price</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                {courses.map( course => {
                    return (
                        <tr key={course._id}>
                            <td><Link href="/courses.[id]" as={`/courses/${course._id}`}>
                                    <a>{course.name}</a>
                                </Link>
                            </td>
                            <td>{course.isActive===true ? 'Active' : 'Inactive'}</td>
                            <td>{course.description}</td>
                            <td>{course.price}</td>
                            <td><UpdateButton courseId={course._id}/> <ArchiveButton courseId={course._id} isActive={course.isActive} /></td>
                        </tr>
                    )
                })}
            </tbody>
        </Table>
    )
}

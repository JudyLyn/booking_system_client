import { useContext } from 'react'
import { Card } from 'react-bootstrap'
import EnrollmentButton from './EnrollmentButton'
import UserContext from '../UserContext'

export default function CourseCard({courseData}) {
    console.log(courseData)
    const { user } = useContext(UserContext)
    return (
        <Card>
            <Card.Body>
                <Card.Title>{courseData.name}</Card.Title>
                <Card.Subtitle>{courseData.description}</Card.Subtitle>
                <Card.Text>Price: PhP{courseData.price}</Card.Text>
                {user.id !== null ? <EnrollmentButton courseId={courseData._id} /> : null}
            </Card.Body>
        </Card>
    )
}

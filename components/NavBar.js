import { useContext } from 'react'
import { Navbar, Nav } from 'react-bootstrap'
import Link from 'next/link'
import UserContext from '../UserContext'

export default function NavBar() {
    //access the global user state via the UserContext
    const { user } = useContext(UserContext)

    return (
        <Navbar bg="light" expand="lg">
            <Link href="/">
                <a className="navbar-brand">B61 Booking</a>
            </Link>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mr-auto">
                    <Link href="/courses">
                        <a className="nav-link" role="button">Course Catalogue</a>
                    </Link>

                    {(user.id !== null)
                    ? (user.isAdmin === true)
                        ?<>
                            <Link href="/courses/create">
                                <a className="nav-link" role="button">Create a Course</a>
                            </Link>
                            <Link href="/logout">
                                <a className="nav-link" role="button">Logout</a>
                            </Link>
                        </>
                        : <Link href="/logout">
                            <a className="nav-link" role="button">Logout</a>
                        </Link>
                    : <>
                        <Link href="/login">
                            <a className="nav-link" role="button">Login</a>
                        </Link>
                        <Link href="/register">
                            <a className="nav-link" role="button">Register</a>
                        </Link>
                    </>}
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    )
}

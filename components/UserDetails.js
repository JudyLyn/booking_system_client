import { Jumbotron, ListGroup } from 'react-bootstrap'
import Link from 'next/link'

export default function UserDetails({userData}) {
    return (
        <>
            <Jumbotron>
                <h1>Name: {`${userData.firstName} ${userData.lastName}`}</h1>
                <h2>Email: {userData.email}</h2>
            </Jumbotron>

            {userData.enrollments.length > 0 
            ? <>
                <h3>Enrollments</h3>
                <ListGroup>
                    {userData.enrollments.map(enrollment => {
                        return (
                            <ListGroup.Item key={enrollment._id}>
                                <Link href="/courses/[id]" as={`/courses/${enrollment.courseId}`}>
                                        <a>{enrollment.courseId}</a>
                                </Link>
                            </ListGroup.Item>
                        )
                    })}
                </ListGroup>
            </>
            : null}
        </>
    )
}

import { Button } from 'react-bootstrap'
import Router from 'next/router'

export default function ArchiveButton({courseId, isActive}) {


	
    const archive = (courseId) => {
        fetch(`http://localhost:4000/api/courses/${courseId}`, {
            method: 'DELETE',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            if(data === true){
                Router.reload()
            }else{
                Router.push('/error')
            }
        })
    }

    if(isActive===true){
        return <Button variant="danger" onClick={()=>archive(courseId)}>Archive</Button>
    }else{
        return <Button variant="success" onClick={()=>archive(courseId)}>Reactivate</Button>
    }
}

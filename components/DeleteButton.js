import { useState } from 'react'
import { Button } from 'react-bootstrap'
import Router from 'next/router'

export default function DeleteButton({courseId}) {

	const [notify, setNotify] = useState(false)

     function deleteCourse(e){
    	e.preventDefault()

    	fetch(`http://localhost:4000/api/courses/${courseId}`, {
    		method: 'DELETE',
    		headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            },
    	})
    	.then(res => res.json())
    	.then(data =>{
    		  if(data===true){
                Router.push('/courses')
            }else{
              setNotify(true)
            }
    	})
    }
    return (
    	<>
        <Button variant="danger" onClick={(e) => deleteCourse(e)}>Delete</Button>


        {notify === true
            ? <Alert variant="danger">Failed to delete a course!</Alert>
            : null}
    	</>
    )
}

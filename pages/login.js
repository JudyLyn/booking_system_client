import { useState, useEffect, useContext } from 'react'
import { Form, Button } from 'react-bootstrap'
import UserContext from '../UserContext'
import Router from 'next/router'
import { GoogleLogin } from 'react-google-login'

export default function login() {
    //use the UserContext and destructure it to obtain the setUser state setter from our application entry point
    const { setUser } = useContext(UserContext)
    //states for form input
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')

    //Google Login
    const captureLoginResponse = (response) => {
        console.log(response)
        const payload = {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({tokenId: response.tokenId})
        }
        fetch('http://localhost:4000/api/users/verify-google-id-token', payload)
        .then((response) => response.json())
        .then(data => {
            //if the data return by the api has an accesstoken, we'll set the localStorage
            if (typeof data.accessToken !== 'undefined') {
                localStorage.setItem('token', data.accessToken)
                setUser({id: data._id, isAdmin: data.isAdmin})
                Router.push('/courses')
            }else {
            //else, if the data return by the api is an error msgs
            if(data.error == 'google-auth-error') {
                alert("Error! Google authentication procedure failed.")
                //error == 'google auth error', return a msg
            }else if(data.error === 'login-type-error'){
                alert("Error! You may have registered through a different login procedure.")
                //error == 'login type error', return a msg
            }


            }
        })
    }


    //send fetch request to the API endpoint responsible for user authentication
    function authenticate(e){
        //prevent redirection via form submission 
        e.preventDefault()

        fetch('http://localhost:4000/api/users/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            //successful authentication will return a JWT
            if(data.accessToken){
                //store this JWT in local storage
                localStorage.setItem('token', data.accessToken)
                //obtain user id and isAdmin properties for setting in our globally scoped user state by sending a fetch request to the appropriate API endpoint
                fetch(`http://localhost:4000/api/users/details`, {
                    headers: {
                        Authorization: `Bearer ${data.accessToken}` 
                    }
                })
                .then(res => res.json())
                .then(data => {
                    //set the global user state to have the proper id and isAdmin properties
                    setUser({
                        id: data._id,
                        isAdmin: data.isAdmin
                    })
                    Router.push('/courses')
                })
            }else{
                //authentication failure, redirect to error page
                Router.push('/error')
            }
        })
    }

    return (
        <>
        <Form onSubmit={(e) => authenticate(e)}>
            <Form.Group>
                <Form.Label>Email</Form.Label>
                <Form.Control type="email" value={email} onChange={(e)=>setEmail(e.target.value)} />
            </Form.Group>
            <Form.Group>
                <Form.Label>Password</Form.Label>
                <Form.Control type="password" value={password} onChange={(e)=>setPassword(e.target.value)} />
            </Form.Group>
            <Button variant="success" type="submit">Submit</Button>
        </Form>
        <p> or </p>
        <GoogleLogin  
        clientId='471999445841-o1j9p977toka5kelhevugsrri6j06uuf.apps.googleusercontent.com' 
        render={renderProps => (
            <Button variant="outline-success" onClick={renderProps.onClick} disabled={renderProps.disabled}>Login using Google Account</Button>
        )}       
        onSuccess={captureLoginResponse} 
        onFailure={captureLoginResponse} 
        cookiePolicy={'single_host_origin'}/>

        </>
    )
}

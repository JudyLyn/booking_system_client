import CourseDetails from '../../components/CourseDetails'
import { useRouter } from 'next/router'
import { Spinner } from 'react-bootstrap'


export default function user({userData}) {
    const router = useRouter()
    //check if this dynamically routed page is currently on fallback via the isFallback property of the router object
    if(router.isFallback){
        return <><span>Fetching user details from API...</span><Spinner animation="border" variant="primary" /></>
    }else{
        return <UserDetails userData={userData} />
    }
}


export async function getStaticPaths() {
    const res = await fetch('http://localhost:4000/api/users/ids')
    const data = await res.json()
    const paths = data.map(datum => {
        return {
            params: {
                id: datum
            }
        }
    })

    return {
        paths,
        fallback: true
    }
}

export async function getStaticProps({ params }) {
    const res = await fetch(`http://localhost:4000/api/users/${params.id}`)
    const userData = await res.json()
    return {
        props: {
            userData
        }
    }
}

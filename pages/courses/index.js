import { useState, useEffect, useContext } from 'react'
import { Jumbotron, Row, Col } from 'react-bootstrap'
import CourseCard from '../../components/CourseCard'
import AdminCourseTable from '../../components/AdminCourseTable'
import UserContext from '../../UserContext'

export default function index({data}) {
    const { user } = useContext(UserContext)
    const [activeCourses, setActiveCourses] = useState([])

    useEffect(() => {
        if(data.length > 0){
            setActiveCourses(data.filter(course => course.isActive === true))
        }
    }, [data])

    if(data.length === 0){
        return (
            <Jumbotron>
                <h1>There are no available courses yet</h1>
            </Jumbotron>
        )
    }else{
        if(user.isAdmin === true){
            return <AdminCourseTable courses={data} />
        }else{
            if(activeCourses.length > 0){
                return (
                    <Row>
                        {activeCourses.map(course => {
                            return (
                            <Col xs={12} md={3} key={course._id}>
                                <CourseCard courseData={course} className="courseCards"/>
                            </Col>
                            )
                        })}
                    </Row>
                )
            }else{
                return (
                    <Jumbotron>
                        <h1>There are no courses on offer as of the moment.</h1>
                    </Jumbotron>
                )
            }
        }
    }
}

//pre-fetch data for pre-rendering our component
//use getServerSideProps so that the fetch request will be sent to the API on every request sent to this page
export async function getServerSideProps() {
    const res = await fetch('http://localhost:4000/api/courses')
    const data = await res.json()

    //use the pre-fetched data as a prop to this component that will be pre-rendered
    return {
        props: {
            data
        }
    }
}

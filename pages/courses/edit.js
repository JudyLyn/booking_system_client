import { useState, useEffect } from 'react'
import { Form, Button, Alert, Spinner } from 'react-bootstrap'
import Router from 'next/router'
import { useRouter } from 'next/router'

export default function edit() {
    const router = useRouter()
    const { courseId } = router.query
    console.log(courseId)

    const [name, setName] = useState('')
    const [description, setDescription] = useState('')
    const [price, setPrice] = useState(100)
    const [isActive, setIsActive] = useState(false)
    const [notify, setNotify] = useState(false)

    //retrieve course information on component getting mounted
    useEffect(() => {
        fetch(`http://localhost:4000/api/courses/${courseId}`)
        .then(res => res.json())
        .then(data => {
            setName(data.name)
            setDescription(data.description)
            setPrice(data.price)
        })
    }, [])


    useEffect(() => {
        if(name.length < 50 && description.length < 200){
            setIsActive(true)
        }else{
            setIsActive(false)
        }
    }, [name, description])

    function editCourse(e){
        e.preventDefault()

        fetch(`http://localhost:4000/api/courses/${courseId}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                name: name,
                description: description,
                price: price
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data===true){
                Router.push('/courses')
            }else{
                setNotify(true)
            }
        })
    }

    return (
        <>
            {name === ''
            ? <><h1>Retrieving course info...</h1></>
            : 
            <Form onSubmit={(e) => editCourse(e)}>
                <Form.Group>
                    <Form.Label>Course Name:</Form.Label>
                    <Form.Control type="text" value={name} onChange={e => setName(e.target.value)} required />
                    {name.length >= 50 ? <Alert variant="warning">Name has exceeded maximum length</Alert>:null}
                </Form.Group>
                <Form.Group>
                    <Form.Label>Description:</Form.Label>
                    <Form.Control as="textarea" rows="3" value={description} onChange={e => setDescription(e.target.value)} required />
                    {description.length >= 200 ? <Alert variant="warning">Description has exceeded maximum length</Alert>:null}
                </Form.Group>
                <Form.Group>
                    <Form.Label>Price:</Form.Label>
                    <Form.Control type="number" value={price} min={100} onChange={e => setPrice(e.target.value)} required />
                </Form.Group>

                {isActive===true
                ? <Button type="submit" variant="success">Update</Button>
                : <Button disabled type="submit" variant="success">Update</Button>}
            </Form>
        }

            {notify === true
            ? <Alert variant="danger">Failed to update course!</Alert>
            : null}

        </>
    )
}

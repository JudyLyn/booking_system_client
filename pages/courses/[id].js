import CourseDetails from '../../components/CourseDetails'
import { useRouter } from 'next/router'
import { Spinner } from 'react-bootstrap'


//component default export
export default function course({ courseData }) {
	const router = useRouter()
	//check if this dynamically routed page is currently on fallback vie the isFallback property of the router object
	if(router.isFallback){
		return <> <span> Fetching course details from API...</span> <Spinner animation="border" variant="primary"/></> 
				
	}else {
		return <CourseDetails courseData={courseData}/>
	}

}

//predefined function
//get all possible ID values for the courses collection
//as is. follow documentation. defining path to pre render during the build timecountry=${params.id}
export async function getStaticPaths(){
	const res = await fetch('http://localhost:4000/api/courses')
	const data = await res.json()
	//create a new array named paths composed of objects wit the following data structure:
	const paths = data.map(datum => {
		return{
			params: {
				id: datum._id
			}
		}
	})
	//return an object with paths as a property alongside a property named fallback
		//fallback is by default set to false
	return {
		paths,
		//fallback false == NextJS will return a default 404 page not found error when an ID URL parameter does not match any of the paths generated
		//fallback true == Next JS will reFETCH or RESEND the fetch request defined in the getStaticProps ONCE to check if it will retrieve data using the id received from the URL parameter
			//if necessary data was obtained, render the component accordingly, else, render default 404 Page Not Found error page
		fallback: true
	}
}

//iterates over all the possible ID's returned by getStaticPaths(), fetching the course details from API for every ID iterated.
//Next.js then pre renders All course pages using this method
//  fetches the data of a course with ID matching the argument passed in to this function.
export async function getStaticProps({ params }){
	const res = await fetch(`http://localhost:4000/api/courses/${params.id}`)
	const courseData = await res.json()
	return {//props cant be change
		props: {
			courseData//can change its name
		}
	}
}
import { useState, useEffect, useContext } from 'react'
import { Form, Button } from 'react-bootstrap'
import Router from 'next/router'
import UserContext from '../UserContext'
import { GoogleLogin } from 'react-google-login'

export default function register() {

    const { setUser } = useContext(UserContext)
    //form input state hooks
    const [email, setEmail] = useState('')
    const [password1, setPassword1] = useState('')
    const [password2, setPassword2] = useState('')
    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [mobileNumber, setMobileNumber] = useState('')

    //state to determine the status of the form submission button
    const [isActive, setIsActive] = useState(false)

    //for any functionality that you want triggered based on a change of state, the useEffect hook is the tool of choice
    useEffect(() => {
        //additional validations for the password and mobile no fields
        if((password1 !== '' && password2 !== '') && (password2 === password1) && (mobileNumber.length === 10)){
            setIsActive(true)
        }else{
            setIsActive(false)
        }
    }, [password1, password2, mobileNumber])



 const captureLoginResponse = (response) => {
        console.log(response)
        const payload = {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({tokenId: response.tokenId})
        }
        fetch('http://localhost:4000/api/users/verify-google-id-token', payload)
        .then((response) => response.json())
        .then(data => {
            //if the data return by the api has an accesstoken, we'll set the localStorage
            if (typeof data.accessToken !== 'undefined') {
                localStorage.setItem('token', data.accessToken)
                setUser({id: data._id, isAdmin: data.isAdmin})
                Router.push('/courses')
            }else {
            //else, if the data return by the api is an error msgs
            if(data.error == 'google-auth-error') {
                alert("Error! Google authentication procedure failed.")
                //error == 'google auth error', return a msg
            }else if(data.error === 'login-type-error'){
                alert("Error! You may have registered through a different login procedure.")
                //error == 'login type error', return a msg
            }


            }
        })
    }


    //function for submitting user registration form
    function registerUser(e){
        e.preventDefault()

        //send fetch request to API endpoint responsible for creating a new user
        fetch('http://localhost:4000/api/users', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                firstName: firstName,
                lastName: lastName,
                email: email,
                password: password1,
                mobileNumber: mobileNumber
            })
        })
        .then(res => res.json())
        .then(data => {
        	if(data === true){
        		Router.push('/login')
        	}else{//redirect to an error page otherwise
        		Router.push('error')
        	}
        })
    }

    return (
        <>
        <Form onSubmit={(e) => registerUser(e)}>
            <Form.Group>
                <Form.Label>First Name</Form.Label>
                <Form.Control type="text" value={firstName} onChange={e => setFirstName(e.target.value)} placeholder="Juan" required></Form.Control>
            </Form.Group>
            <Form.Group>
                <Form.Label>Last Name</Form.Label>
                <Form.Control type="text" value={lastName} onChange={e => setLastName(e.target.value)} placeholder="Dela Cruz" required></Form.Control>
            </Form.Group>
            <Form.Group>
                <Form.Label>Email</Form.Label>
                <Form.Control type="email" value={email} onChange={e => setEmail(e.target.value)} placeholder="juan@mail.com" required></Form.Control>
            </Form.Group>
            <Form.Group>
                <Form.Label>Mobile Number</Form.Label>
                <Form.Control type="text" value={mobileNumber} onChange={e => setMobileNumber(e.target.value)} placeholder="9xxxxxxxx" required></Form.Control>
            </Form.Group>
            <Form.Group>
                <Form.Label>Password</Form.Label>
                <Form.Control type="password" value={password1} onChange={e => setPassword1(e.target.value)} placeholder="Your password should be atleast 8 characters"  required></Form.Control>
            </Form.Group>
            <Form.Group>
                <Form.Label>Verify Password</Form.Label>
                <Form.Control type="password" value={password2} onChange={e => setPassword2(e.target.value)} required></Form.Control>
            </Form.Group>
            {isActive === true 
            ? <Button variant="success" type="submit">Submit</Button> 
            : <Button variant="success" type="submit" disabled>Submit</Button>}
        </Form>

                <p> or </p>
        <GoogleLogin  
        clientId='471999445841-o1j9p977toka5kelhevugsrri6j06uuf.apps.googleusercontent.com' 
        render={renderProps => (
            <Button variant="outline-success" onClick={renderProps.onClick} disabled={renderProps.disabled}>Register using Google Account</Button>
        )}       
        onSuccess={captureLoginResponse} 
        onFailure={captureLoginResponse} 
        cookiePolicy={'single_host_origin'}/>

        </>
    )
}

import { useState, useEffect } from 'react'
import '../styles/globals.css'
import 'bootstrap/dist/css/bootstrap.min.css'
//import the context provider
import { UserProvider } from '../UserContext'
import { Container } from 'react-bootstrap'
import NavBar from '../components/NavBar'
import Head from 'next/head'

function MyApp({ Component, pageProps }) {
  //initialize a globally available user state as an object with id and isAdmin properties both set to null
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  //function for clearing local storage upon logout
  const unsetUser = () => {
    //removes the JWT that was stored in the local storage of app
    localStorage.clear()
    //set the global user state properties to null
    setUser({
      id: null,
      isAdmin: null
    })
  }

  //effect hook that will get user details from the API whenever the user state's id property changes
  useEffect(() => {
    fetch('http://localhost:4000/api/users/details', {
      headers: {
        //the user info that will be retrieved from the API depends on the value of the encoded JWT
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    //parse the response into desired file type
    .then(res => res.json())
    .then(data => {
      console.log(data)
      //if JWT is valid set user id and isAdmin states accordingly
      if(data._id){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      }else{//else, keep both id and isAdmin properties null
        setUser({
          id: null,
          isAdmin: null
        })
      }      
    })
  }, [user.id])

  return (
    // make the user state, setUser state setter, and unsetUser function globally available in our app by passing them in as values to the context provider
    <>
    <Head>
      <title>B61 Booking</title>
    </Head>
    {/* context providers have to wrap the entire application's component tree so that any component in the tree can subscribe to changes in the global values being provided */}
    <UserProvider value={{user, setUser, unsetUser}}>
      <NavBar />
      <Container>
        <Component {...pageProps} />
      </Container>
    </UserProvider>
    </>
  )
}

export default MyApp
